use std::io::stdin;

fn is_isbn10() -> bool{
    // crear y comentar esta función
    
    return true
}

fn is_isbn_format_valid(c: &str) -> bool {

    // comentar esta funcion
    // para verificar que el valor ingresado en c es valido, se condiciona para validar cada caracter en c
    // returna un true de ser correcto
    if c.chars().next().unwrap().is_numeric() {
        return true;
        // tambien returna true si existe el caracter X en el valor entregado
    } else if c == "X" || c == "x"{
        return true;
    }
}

fn main(){

    let mut isbn: String = String::new();
    let mut clean_isbn: String = String::new();
    stdin().read_line(&mut isbn).unwrap();
   
   // delimitacion para isbn de 10 caractere
    if isbn.len() != 10{
        return false;
    }
    // comentar este ciclo
    // por cada caracter en c se condiciona para isbn en un string
    // donde se limpia de cualquier espacio en blanco
    for c in isbn.to_string().trim().chars(){
        // si el formato es valido entonces la version limpia de isbn sera igual al valor ingresado anteriormente
        if is_isbn_format_valid(&c.to_string()){
            clean_isbn = clean_isbn + &c.to_string(); 
        }
    }

    println!("{}", clean_isbn);
    // comentar esta sentencia
    // si el formato de isbn ingresado es igual a la version limpia de caracteres y espacios, este programa
    // imprime que el valor isbn-10 es correcto
    if is_isbn10(clean_isbn){
        println!("{} es un ISBN10 valido", clean_isbn);
    } else {
        println!("No lo es");
    }

}
